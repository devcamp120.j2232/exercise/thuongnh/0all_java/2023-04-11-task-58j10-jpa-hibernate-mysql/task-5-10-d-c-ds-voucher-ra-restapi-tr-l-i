package com.devcamp.jpasql.controller;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.weaver.patterns.IVerificationRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jpasql.model.CVoucher;
import com.devcamp.jpasql.repository.IVoucherRepository;



@CrossOrigin
@RestController
@RequestMapping("/")

public class CVoucherController {
    @Autowired
    IVoucherRepository pIVoucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity <List<CVoucher>> getAllVoucher(){
        try {
            List<CVoucher> pVouchers = new ArrayList<CVoucher>();
            pIVoucherRepository.findAll().forEach(pVouchers::add);
            return new ResponseEntity<>(pVouchers, HttpStatus.OK);
            
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
