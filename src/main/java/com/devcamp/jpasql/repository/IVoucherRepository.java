package com.devcamp.jpasql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.jpasql.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}
